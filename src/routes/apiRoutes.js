const router = require("express").Router();
const alunoController = require("../controller/alunoController");
const JSONPlaceholderController = require("../routes/JSONPlaceholderController")

router.post("/cadastro/", alunoController.postAluno);
router.get("/aluno/", alunoController.getAluno);
router.put("/editar/", alunoController.updateAluno);
router.delete("/delete/:id", alunoController.deleteAluno);

router.get("/external", JSONPlaceholderController.getUsers);

module.exports = router;
